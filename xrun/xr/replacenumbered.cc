#include "xr.ih"

void XR::replaceNumbered(string *ret, unsigned pos)
{
    unsigned idx = (*ret)[pos + 1] - '0';

    if (idx == 0 || idx > 9)
    {
        if (!d_quiet)
            cout << "won't replace `%" << *(ret + pos + 1) << "'" <<
                "(char #" << pos + 1 << ") in\n" <<
                *ret << endl;
        (*ret)[pos] = '?';
        return;
    }

    if (idx <= d_arg.nArgs())
    {
        ret->replace(pos, 2, d_arg[idx - 1]);
        return;
    }

    if (!d_quiet)
        cout << "argument `%" << idx << "' "
                "(char #" << pos + 1 << ") unavailable for\n" <<
                *ret << endl;

    ret->erase(pos, 2);
}





