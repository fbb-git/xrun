#include "xr.ih"

void XR::replaceAll(string *ret, unsigned pos, unsigned idx)
{
    string replacement;

    for ( ; idx < d_arg.nArgs(); ++idx)
        (replacement += d_arg[idx]) += " ";

    ret->replace(pos, 2, replacement);
}

