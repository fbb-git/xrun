#include "xr.ih"

string XR::getResource(string const &rc)
{
    auto it = d_config.findRE("^" + rc);

    if (it == d_config.end())
        throw Exception() << d_prog << ": No entry `" << rc << 
                "' in resource file `" << d_configName << '\'';

    return *it;
}
