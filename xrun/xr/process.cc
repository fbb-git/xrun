#include "xr.ih"

void XR::process()
{
    if (d_msg.length())
        throw Exception() << d_msg;

    string resource = getResource(d_arg[0]);

    substituteArguments(&resource);

    ownArguments(&resource);            // remove xrun's arguments

    if (!d_display)
        noDisplay();

    if (d_arg.option('s'))
    {
        cout << d_xterm << ' ' << resource << endl;
        throw 1;
    }

    if (d_mouse)
    {
        Xpointer xpointer;
        xpointer.set(d_x, d_y);
    }
    
    Daemon daemon(d_xterm + " " + resource);
    daemon.fork();
}








