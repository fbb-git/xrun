#include "xr.ih"

void XR::shell(vector<string>::iterator it)
{
    *it++ = "";
    if (!d_arg.option('x'))
        d_xterm = *it;

    *it = "";
}
