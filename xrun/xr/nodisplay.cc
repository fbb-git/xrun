#include "xr.ih"

void XR::noDisplay()
{
    char const **argv;
    vector<string> vs;
    unsigned n;

    if (!d_textCmd.length())
    {
        argv = d_arg.argPointers();
        n = d_arg.nArgs();
    }
    else
    {
        n = String::split(&vs, d_textCmd);
        argv = String::argv(vs);
    }

    if (d_arg.option('s'))
    {
        copy(argv, argv + n, ostream_iterator<char const *>(cout, " "));
        cout << endl;
        throw 1;
    }

    if (d_arg.option('X'))
    {
        if (!d_quiet)
            cout << d_prog << 
                    ": --X-only and no DISPLAY environment variable set.\n"
                    "Can't run `" << argv[0] << "'" << endl;
        throw 1;
    }

    execvp(argv[0], const_cast<char *const *>(argv));

    throw Exception() << Arg::instance().basename().c_str() << 
                        ": Can't execute " << argv[0];
}
