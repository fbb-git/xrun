#ifndef _INCLUDED_XR_
#define _INCLUDED_XR_

#include <string>

#include <bobcat/arg>
#include <bobcat/configfile>

class XR
{
    static char const s_localConfig[];
    static char const s_globalConfig[];
    static char const s_xterm[];
    static unsigned const s_maxReplacements = 20;

    FBB::Arg &d_arg;
    std::string d_configName;
    FBB::ConfigFile d_config;
    char const *d_prog;
    unsigned d_maxReplacements;
    std::string d_xterm;
    std::string d_shellArgs;
    std::string d_textCmd;
    int d_x;
    int d_y;
    bool d_display;
    bool d_mouse;
    bool d_quiet;
    std::string d_msg;

    public:
        XR();

        void process();

        static char const *localConfig()
        {
            return s_localConfig;
        }

        static char const *globalConfig()
        {
            return s_globalConfig;
        }

        static char const *xterm()
        {
            return s_xterm;
        }

        static unsigned maxReplacements()
        {
            return s_maxReplacements;
        }

    private:
        std::string getResource(std::string const &rc);
        void mouseTo(std::string const &coord);
        void shell(std::vector<std::string>::iterator it);
        void noDisplay();
        void ownArguments(std::string *resource);
        void redefMouse(std::vector<std::string>::iterator it);
        void replaceAll(std::string *ret, unsigned pos, unsigned first);
        void replaceNumbered(std::string *ret, unsigned pos);
        void substituteArguments(std::string *resource);
        void textCommand(std::vector<std::string>::iterator it);
        void xOnly(std::vector<std::string>::iterator it);
};

        
#endif
