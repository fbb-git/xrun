#include "xr.ih"

void XR::ownArguments(string *resource)
{
    vector<string> vs;

    String::split(&vs, *resource);

    vs[0] = "";                         // remove the keyword

    for (unsigned idx = 0; idx < vs.size(); idx++)
    {
        if (vs[idx][0] == '-')
        {
            switch (char option = vs[idx][1])
            {
                case 'm':
                    redefMouse(vs.begin() + idx);
                break;
    
                case 't':
                    textCommand(vs.begin() + idx);
                break;
    
                case 'x':
                    shell(vs.begin() + idx);
                break;
    
                case 'X':
                    xOnly(vs.begin() + idx);
                break;
    
                case '-':
                {
                    vs[idx] = "";
                    ostringstream out;
                    copy(vs.begin(), vs.end(), 
                            ostream_iterator<string>(out, " "));
                    *resource = out.str();
                }
                return;
    
                default:
                    throw Exception() << d_prog <<": Option " << option << 
                        " not allowed before --. Only -m, -t, -x and -X "
                        "may be specified";
            }
        }
    }
}

