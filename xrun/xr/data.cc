#include "xr.ih"

char const XR::s_localConfig[]   = ".xrunrc";
char const XR::s_globalConfig[]  = "/usr/share/xrun/xrun.rc";
char const XR::s_xterm[]         = "xterm";
