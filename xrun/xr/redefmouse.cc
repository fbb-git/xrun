#include "xr.ih"

void XR::redefMouse(vector<string>::iterator it)
{
    *it++ =  "";
    
    if (!d_arg.option('m'))
        mouseTo(*it);

    *it = "";
}
