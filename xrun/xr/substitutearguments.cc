#include "xr.ih"

void XR::substituteArguments(std::string *resource)
{
    for (unsigned idx = d_maxReplacements; idx--; )
    {
        string::size_type pos = resource->find('%');

        if (pos == string::npos || resource->length() == pos + 1)
            return;

        switch ((*resource)[pos + 1])
        {
            case 'a':
                replaceAll(resource, pos, 0);
            break;

            case 'A':
                replaceAll(resource, pos, 1);
            break;

            default:
                replaceNumbered(resource, pos);
            break;
        }
    }
}




