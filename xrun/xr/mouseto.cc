#include "xr.ih"

void XR::mouseTo(string const &coord)
{
    if (!d_display)
        return;

    if ((A2x(coord) >> d_x >> d_y).fail())
        throw Exception() << d_prog << ": invalid `--mouse-setting " << 
                        coord << "' specification\n"
                        "(" << d_x << ", " << d_y << ") "
                        "Use `--mouse-setting +x+y'";

    d_mouse = true;
}
