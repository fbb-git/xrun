#include "xr.ih"

void XR::textCommand(vector<string>::iterator it)
{
    *it++ =  "";

    if (!d_arg.option('t'))
        d_textCmd = *it;

    *it = "";
}
