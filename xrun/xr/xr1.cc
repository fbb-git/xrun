#include "xr.ih"

XR::XR()
:
    d_arg(Arg::instance()),
    d_prog(d_arg.basename().c_str()),
    d_display(getenv("DISPLAY")),
    d_mouse(false),
    d_quiet(d_arg.option('q'))
{
    try
    {
        if (!d_arg.option(&d_configName, 'c'))
        {
            User owner;
            owner.verify();
    
            string localname = owner.homedir() + s_localConfig;
    
            d_configName = access(localname.c_str(), F_OK) == 0 ? 
                                              localname : s_globalConfig;
        }
        if (access(d_configName.c_str(), R_OK) != 0)
            throw Exception() << d_prog << "Can't read `" << d_configName << 
                                                                        '\'';
    
        d_config.open(d_configName);
    
        string replacements;
        d_maxReplacements = d_arg.option(&replacements, 'r') ?
                                    A2x(replacements)
                            :
                                    s_maxReplacements;

        string mouse;
        if (d_arg.option(&mouse, 'm'))
            mouseTo(mouse);        
    
        d_arg.option(&d_textCmd, 't');
    
        if (!d_arg.option(&d_xterm, 'x'))
            d_xterm = s_xterm;
    }
    catch (exception const &err)
    {
        d_msg = err.what();
    }
}

