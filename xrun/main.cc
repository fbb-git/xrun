/*
                              xrun.cc
*/

#include "main.ih"

using namespace std;
using namespace FBB;

namespace
{
    Arg::LongOption longOpt[] =
    {
        Arg::LongOption("config-file",      'c'),
        Arg::LongOption("help",             'h'),
        Arg::LongOption("mouse-setting",    'm'),
        Arg::LongOption("max-replacements", 'r'),
        Arg::LongOption("quiet",            'q'),
        Arg::LongOption("show-command",     's'),
        Arg::LongOption("text-mode",        't'),
        Arg::LongOption("version",          'v'),
        Arg::LongOption("xterm",            'x'),
        Arg::LongOption("X-only",           'X'),
    };
    Arg::LongOption const *endOpt = 
        longOpt + sizeof(longOpt) / sizeof(Arg::LongOption);
}

int main(int argc, char **argv)
try
{
    Arg &arg = Arg::initialize("a:c:hm:qr:sS:t:vx:X", longOpt, endOpt, 
                               argc, argv);

    arg.versionHelp(usage, version, 1);

    XR xr;
    xr.process();
}
catch (exception const &err)
{
    cout << err.what() << endl;
    return 1;
}
catch (int x)
{
    return x;
}

