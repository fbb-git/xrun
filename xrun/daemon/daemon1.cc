#include "daemon.ih"

Daemon::Daemon(string const &command)
:
    d_command(command)
{
    String::split(&d_words, command, " \t");
}
