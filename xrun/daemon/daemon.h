#ifndef _INCLUDED_DAEMON_
#define _INCLUDED_DAEMON_

#include <string>
#include <vector>

#include <bobcat/fork>

class Daemon: public FBB::Fork
{
    std::string const &d_command;
    std::vector<std::string> d_words;

    public:
        Daemon(std::string const &command);

    private:
        virtual void parentProcess()
        {}

        virtual void childProcess();
};

        
#endif
