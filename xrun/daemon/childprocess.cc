#include "daemon.ih"

void Daemon::childProcess()
{
    char const **argv = String::argv(d_words);

    execvp(d_words[0].c_str(), const_cast<char *const *>(argv));

    throw Exception() << Arg::instance().basename().c_str() <<
                ": Exec failure when executing\n" << d_command;
}
